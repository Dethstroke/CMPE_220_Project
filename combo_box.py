from alpha_vantage.timeseries import TimeSeries
import matplotlib.pyplot as plt
import pandas
import Tkinter as tk
from Tkinter import *
import ttk
import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import numpy as np
import os, errno
import linearregressionwithplotting
import normalequationwithplotting



ts = TimeSeries(key='6JDC53DOY2SL6TG0', output_format='pandas')

colnames = ['Symbol', 'Name', 'lastsale', 'netchange', 'ptchange', 'share_volume', ' Nasdaq100_points']
symbol_data = pandas.read_csv('nasdaq100list.csv', names = colnames)

option_list = symbol_data.Name.tolist()

master = Tk()
master.minsize(width=666, height=666)


var = StringVar(master)
var.set("Select a company") # initial value
w = ttk.Combobox(master, textvariable=var, values=option_list)
w.pack()

start_month_var = StringVar(master)
start_month_var.set("1")
months_list = range(1, 13)
sm = ttk.Combobox(master, textvariable=start_month_var, values=months_list)
sm.pack()

start_day_var = StringVar(master)
start_day_var.set("1")
days_list = range(1, 32)
sd = ttk.Combobox(master, textvariable=start_day_var, values=days_list)
sd.pack()

start_year_var = StringVar(master)
start_year_var.set("2010")
years_list = range(2000, 2018)
sd = ttk.Combobox(master, textvariable=start_year_var, values=years_list)
sd.pack()


end_month_var = StringVar(master)
end_month_var.set("1")
months_list = range(1, 13)
em = ttk.Combobox(master, textvariable=end_month_var, values=months_list)
em.pack()

end_day_var = StringVar(master)
end_day_var.set("1")
days_list = range(1, 32)
ed = ttk.Combobox(master, textvariable=end_day_var, values=days_list)
ed.pack()

end_year_var = StringVar(master)
end_year_var.set("2016")
years_list = range(2000, 2018)
ed = ttk.Combobox(master, textvariable=end_year_var, values=years_list)
ed.pack()

f = Figure(figsize=(5,5), dpi=100)
a = f.add_subplot(111)
a.plot([],[])
canvas = FigureCanvasTkAgg(f, master)
canvas.show()
canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
toolbar = NavigationToolbar2TkAgg(canvas, master)
toolbar.update()
canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

#def testing_shit():
	#data['close'].plot()
	#print(data.loc['2000-01-03'])
	#print(list(data.index[0]))
	#print(symbol_data)
	#data.to_csv('destroyer.csv', sep='\t')
	#plt.title('Intraday Times Series for the GOOGLE  stock')
	#plt.show()

def ok():
    print "value is", var.get()
    global my_symbol
    my_symbol = symbol_data.Symbol.where(symbol_data.Name == var.get()).max()
    #my_symbol = symbol_data.loc[symbol_data['Name'] == var.get()]['Symbol']
    print(my_symbol)
    global data
    global meta_data
    data, meta_data = ts.get_daily_adjusted(symbol=my_symbol, outputsize='full')

    start_month = int(start_month_var.get())
    start_day = int(start_day_var.get())
    start_year = int(start_year_var.get())

    end_month = int(end_month_var.get())
    end_day = int(end_day_var.get())
    end_year = int(end_year_var.get())



    start_date = str(start_year) + '-' + str(start_month) + '-' + str(start_day)
    
    while True:
        try:
            data.loc[str(start_date)]
        except KeyError:
            print start_date + " not found"
            if start_day<31:
                start_day += 1
            elif start_month < 12:
                start_day = 1
                start_month += 1
            else:
                start_day = 1
                start_month = 1
                start_year += 1
            if start_month < 10:
                string_start_month = '0' + str(start_month)
            else:
                string_start_month = str(start_month)
            if start_day < 10:
                string_start_day = '0' + str(start_day)
            else:
                string_start_day = str(start_day)
            start_date = str(start_year) + '-' + string_start_month + '-' + string_start_day    

        else:
            print 'Index: ' + start_date + 'found'
            break



    end_date = str(end_year) + '-' + str(end_month) + '-' + str(end_day)
    
    while True:
        try:
            data.loc[str(end_date)]
        except KeyError:
            print end_date + " not found"
            if end_day<31:
                end_day += 1
            elif end_month < 12:
                end_day = 1
                end_month += 1
            else:
                end_day = 1
                end_month = 1
                end_year += 1
            if end_month < 10:
                string_end_month = '0' + str(end_month)
            else:
                string_end_month = str(end_month)
            if end_day < 10:
                string_end_day = '0' + str(end_day)
            else:
                string_end_day = str(end_day)
            end_date = str(end_year) + '-' + string_end_month + '-' + string_end_day    

        else:
            print 'Index: ' + end_date + 'found'
            break


    data = data[start_date:end_date]
    try:
        os.remove('Data.csv')
    except OSError as e: # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occurred
    data.to_csv('Data.csv', sep=',')
    a.clear()
    data['close'].plot(ax = a)
    canvas.show()
    print "Data Scrapping done!"

def quit():
    master.quit()

def run_linear():
    linearregressionwithplotting.main()
def run_normal():
    normalequationwithplotting.main()
    

linear_button = Button(master, text="Linear Regression Model", command=run_linear)
normal_button = Button(master, text="Normal Equation Model", command=run_normal)
button = Button(master, text="OK", command=ok)
quit_button = Button(master, text="Quit", command=quit)
quit_button.pack()
button.pack()
linear_button.pack()
normal_button.pack()


mainloop()
