import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import numpy as np
import re
import sys
import codecs
import xlrd
import random
import os
from tkinter import *
theta=0
predictedoutputarray=[]
derivativecostfunction=0
def makeMatrix(I, J, fill=0.0):
    m = []
    for i in range(I):
        m.append([fill]*J)
    return m
def initialtrainer(inputs):
        x=makeMatrix(len(inputs),5)
        y=makeMatrix(len(inputs),1)
        global theta
        global derivativecostfunction
        for i in range (len(inputs)):
            x[i]=inputs[i][0]
            y[i]=inputs[i][1]
        Y=np.array(y)
        X=np.array(x)
        sqmatrixX=np.linalg.inv(np.dot(X.transpose(),X))
        temp=np.dot(sqmatrixX,X.transpose())
        theta=np.dot(temp,Y)
        for i in range (len(theta)):
            theta[i]= theta[i] + 0.0053

        costfunction=hypothesistrainer(X,Y,theta)
        for j in [10]:
            for i in range (len(theta)):

                theta[i]=theta[i]-0.00001*derivativecostfunction

            costfunction=hypothesistrainer(X,Y,theta)
            print ('CF',costfunction)
        return 0
        #return costfunction

def hypothesistrainer(inputs2,outputs2,o):
        hypothesisfunction=0.0
        costfunction=0.0
        x=inputs2
        y=outputs2
        theta=o
        global derivativecostfunction
        for i in range (len(x)):

              hypothesisfunction=np.dot(theta.transpose(),x[i])
              costfunction+=(hypothesisfunction-y[i])**2
              derivativecostfunction+=hypothesisfunction-y[i]
              #print(hypothesisfunction) #predictedvalue
        costfunction=costfunction/(2*len(inputs2))
        derivativecostfunction=derivativecostfunction/len(inputs2)
        return costfunction


def tester(testdata):
        x=makeMatrix(len(testdata),len(testdata[1][0]))
        y=makeMatrix(len(testdata),len(testdata[1][1]))
        global theta
        for i in range (len(testdata)):
            x[i]=testdata[i][0]
            y[i]=testdata[i][1]
        Y=np.array(y)
        X=np.array(x)
        errorterm=0
        for i in range (len(x)):
            predictedoutput=np.dot(theta.transpose(),X[i])
            errorterm+=(predictedoutput-Y[i])**2
            predictedoutputarray.append(predictedoutput)
            #print(predictedoutput)
        errorterm=errorterm/(len(testdata)-4)
        
        return errorterm, X[len(X) -1]
def plotfunction(inputs):
    inputs=inputs[-len(predictedoutputarray):]
    fig, ax = plt.subplots()
    ax.plot(inputs.date, inputs.close, 'o-')
    fig.autofmt_xdate()
    ax.set_title('Actual(B) v/s Predicted(G) Close Price')
    ax.set_xlabel('Days')
    ax.set_ylabel('Closing Price')
    
    for i in range (len(predictedoutputarray)):
        inputs.close[i]=predictedoutputarray[i]
    
    ax.plot(inputs.date, inputs.close, 'o-')   
    plt.show()
    return 0

def train(inputs1):
        x=inputs1
        error=initialtrainer(x)
        #print('error after training= %-.5f' % error)

def predictfuture(xi):

    x=np.dot(theta.transpose(),xi)
    root = Tk()
    T = Text(root, height=2, width=30)
    T.pack()
    T.insert(END, "The next Day'spredicted close price is: $ " + str(x[0]))
    mainloop()
    
    
def main():
    cwd = os.path.dirname(os.path.abspath(__file__)) + '\Data_1.csv'
    datafile = cwd
    print ('loading %s' % datafile)
    original = mlab.csv2rec(datafile)
    
    
    close=original.close
    adjclose=original.adjusted_close
    high=original.high
    low=original.low
    Open=original.open

    pat=[]
    #pat=[[[124.88,124.92,122.50,122.77],[124.88]],[[114.86,118.12,115.31,117.63],[115.31]],[[105.84,107.43,104.63,106.54],[106.26]]]
    for i in range(len(adjclose)-1):
        pat.append([[adjclose[i],high[i],low[i],Open[i]], [close[i+1]]])
    #to train the NN
        pattrain=[]
        pattest=[]
    x = close[len(close)-3]
    
    for i in range (len(pat)):
        if(i<=len(pat)*2/3):
            pattrain.append(pat[i])
        else:
            pattest.append(pat[i])
    train(pattrain)
    e,xi=tester(pattest)
    print(e)
    plotfunction(original)
    
    train(pat)
    predictfuture(xi)
    
if __name__ == '__main__':
    main()
