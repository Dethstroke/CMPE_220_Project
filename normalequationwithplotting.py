import numpy as np
import re
import sys
import codecs
import math
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.cbook as cbook
import os
from tkinter import *

epsilone0=0
TFpoles=0
autocorrelation=0
b0=0
predictedoutputarray=[]

def makeMatrix(I, J, fill=0.0):
    m = []
    for i in range(I):
        m.append([fill]*J)
    return m



def autoregression(inputs):
        global autocorrelation
        global epsilone0
        global TFpoles
        x=makeMatrix(len(inputs),5)
        y=makeMatrix(len(inputs),1)
        for i in range (len(inputs)):
            x[i]=inputs[i][0]
            y[i]=inputs[i][1]
        Y=np.array(y)
        X=np.array(x)
        sqmatrixX=np.linalg.inv(np.dot(X.transpose(),X))
        temp=np.dot(sqmatrixX,X.transpose())
        TFpoles=np.dot(temp,Y)
        for i in range (len(TFpoles)):
            TFpoles[i]= TFpoles[i] - 0.005
        

            

def TFcalculator():
    global autocorrelation
    global epsilone0
    global TFpoles
    global b0
    temp2=np.dot(autocorrelation,TFpoles)
    b0square=0
    for i in range (len(temp2)):
        b0square+=temp2[i]
    if(b0square<0):
       b0=math.sqrt(b0square)
    else:
       b0=math.sqrt(b0square)
    print('TF gain:',b0)
    return 0

def test(testdata):
        global TFpoles
        global epsilone0
        global b0
        global predictedoutputarray
        errorterm=0.0
        x=makeMatrix(len(testdata),len(testdata[1][0]))
        y=makeMatrix(len(testdata),len(testdata[1][1]))
        
        for i in range (len(testdata)):
            x[i]=testdata[i][0]
            y[i]=testdata[i][1]
        Y=np.array(y)
        X=np.array(x)
        for i in range (len(x)):

          predictedoutput=(np.dot(TFpoles.transpose(),X[i]))
          errorterm+=(predictedoutput-Y[i])**2
          predictedoutputarray.append(predictedoutput)
          #print('predictedoutput:',predictedoutput)
        errorterm=errorterm/(2*len(testdata))
        print(len(predictedoutputarray))
        print(len(Y))
        return errorterm, X[len(X) -1]
    
def plotfunction(inputs):
    global predictedoutputarray
    inputs=inputs[-len(predictedoutputarray):]
    fig, ax = plt.subplots()
    ax.plot(inputs.date, inputs.close, 'o-')
    fig.autofmt_xdate()
    ax.set_title('Actual(B) v/s Predicted(G) Close Price')
    ax.set_xlabel('Days')
    ax.set_ylabel('Closing Price')
    
    for i in range (len(predictedoutputarray)):
        inputs.close[i]=predictedoutputarray[i]
    
    ax.plot(inputs.date, inputs.close, 'o-')   
    plt.show()
    return 0

def predictfuture(xi):
    
    x=np.dot(TFpoles.transpose(),xi)
    root = Tk()
    T = Text(root, height=2, width=30)
    T.pack()
    T.insert(END, "The next Day'spredicted close price is: $ " + str(x[0]))
    mainloop()            


def main():
    cwd = os.path.dirname(os.path.abspath(__file__)) + '\Data_1.csv'
    print (cwd)

    datafile = cwd
    print ('loading %s' % datafile)
    original = mlab.csv2rec(datafile)

    original.sort()
    close=original.close
    adjclose=original.adjusted_close
    high=original.high
    low=original.low
    oopen=original.open

    Close=[]
    Adjclose=[]
    High=[]
    Low=[]
    Open=[]
    for i in range (len(close)-1):
        Close.append(close[i+1])
    for i in range (len(adjclose)-1):
        Adjclose.append(adjclose[i])
        High.append(high[i])
        Low.append(low[i])
        Open.append(oopen[i])
    


    
    pat=[]
    pattrain=[]
    pattest=[]
    
    #pat=[[[124.88,124.92,122.50,122.77],[124.88]],[[114.86,118.12,115.31,117.63],[115.31]],[[105.84,107.43,104.63,106.54],[106.26]]]
    for i in range(len(Adjclose)):
        pat.append([[Adjclose[i],High[i],Low[i],Open[i],close[i]], [Close[i]]])

    #to train the NN
    for i in range (len(pat)):
        if(i<=len(pat)*2/3):
             pattrain.append(pat[i])
        else:
             pattest.append(pat[i])
    autoregression(pattrain)
    TFcalculator()
    
    
    e,xi=test(pattest)
    print(e)
    plotfunction(original)
    
    predictfuture(xi)

    
if __name__ == '__main__':
    main()
    

